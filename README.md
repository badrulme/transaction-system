# SELISE assignment for Java Software Engineer

## Details:
_**This project required JDK 11.**_
#### Step1
Open this project into your favourite Java IDE i.e. Intellij iDea

#### Step2
Run below SQL code into your DB
```sql
SET foreign_key_checks = 0;
drop table if exists ACCOUNT;

create table ACCOUNT
(
  id             int primary key auto_increment,
  name           varchar(255) not null,
  address        varchar(255),
  account_number varchar(255) not null unique,
  balance        double       not null,
  status         tinyint(1)   not null default false
);

insert into ACCOUNT (name, address, account_number, balance, status)
values ('Arifur Rahman', 'Dhaka, Bangladesh', '001241009211439', 5000, true);

insert into ACCOUNT (name, address, account_number, balance, status)
values ('Rakinul Huq', 'Dhaka, Bangladesh', '32341200923487', 5000, true);

insert into ACCOUNT (name, address, account_number, balance, status)
values ('Md. Badrul Alam', 'Jashore, Bangladesh', '123456789123456', 5000, true);

drop table if exists TRANSACTION;
create table TRANSACTION
(
  id                         int primary key auto_increment,
  request_id                 varchar(255) not null unique,
  transaction_time           datetime,
  requester                  varchar(255) not null,
  transaction_type           varchar(10) not null,
  source_account_number      int          not null,
  amount                     double,
  destination_account_number int          not null,
  note                       varchar(255) not null,
  foreign key TRANSACTION_FK_01 (source_account_number) references ACCOUNT (id),
  foreign key TRANSACTION_FK_02 (destination_account_number) references ACCOUNT (id)
);

SET foreign_key_checks = 1;
```

## Endpoint
***
**1.1 Get Application statusRequest**
### Request
Request method: GET
Request url: http://localhost:8080/sayRunning
### Response
Expected response of this request could be this type of format.

**Code: 200**

`Application is running...`
***
**1.2 Transaction API**

### Request

Request with Transaction information.

Request URL: http://localhost:8080/transaction

**Request method:** POST

**Request Body**
```json
    {
        "requestId": "A32W4ER2341",
        "requester": "XYZ App",
        "transactionType": "VFJBTlNGRVI=",
        "sourceAccountNumber": "MzIzNDEyMDA5MjM0ODc=",
        "amount": "MTUwMC41MA==",
        "destinationAccountNumber": "MDAxMjQxMDA5MjExNDM5",
        "note": "Transferring amount"
    }
```

### Response
The expected response of this request could be this type of format.

**Code: 200**

**Code: 400**

```json
{
    "message": [
        "requester must not be blank",
        "requestId must not be blank",
        "transactionType should be either TRANSFER or REVERSE"
    ],
    "status": "error"
}
```
```json
{
    "status": "error",
    "message": "requestId already exists!"
}
```

```json
{
    "status": "error",
    "message": "Amount conversion failed for : vd04D00"
}
```

```json
{
    "status": "error",
    "message": "source account does not have sufficient balance!"
}
```
```json
{
    "status": "error",
    "message": "destination account does not have sufficient balance!"
}
```

***
## For more details
- [First section Question & Ans](https://nahalit.atlassian.net/wiki/spaces/SA/pages/64127207/Assignment+1)
- [Assignment 1](https://nahalit.atlassian.net/wiki/spaces/SA/pages/64127207/Assignment+1)
- [Assignment 2](https://nahalit.atlassian.net/wiki/spaces/SA/pages/64192516/Assignment+2)
  - [API Spec](https://nahalit.atlassian.net/wiki/spaces/SA/pages/64192575/API+Spec)


