package ch.selise.transactionmanager.model;

import ch.selise.transactionmanager.validator.IsValidTransactionType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class TransactionRequest {
    @NotBlank
    private String requestId;
    @NotBlank
    private String requester;
    @NotBlank
    @IsValidTransactionType
    private String transactionType;
    @NotBlank
    private String sourceAccountNumber;
    @NotBlank
    private String amount;
    @NotBlank
    private String destinationAccountNumber;
    @NotBlank
    private String note;
}
