package ch.selise.transactionmanager.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Application Status Controller
 *
 * @author Badrul
 */
@RestController
public class ApplicationStatusController {
    @GetMapping("/sayRunning")
    public ResponseEntity<String> sayRunning() {
        return new ResponseEntity<>("Application is running...", HttpStatus.OK);
    }
}
