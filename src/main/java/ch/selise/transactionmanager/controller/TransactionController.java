package ch.selise.transactionmanager.controller;

import ch.selise.transactionmanager.model.TransactionRequest;
import ch.selise.transactionmanager.service.TransactionService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Transaction Controller class
 *
 * @author Badrul
 */
@RestController
public class TransactionController {

    private final TransactionService service;

    /**
     * Dependency inject throw constructor autowiring
     *
     * @param transactionService type of TransactionService
     */
    public TransactionController(TransactionService transactionService) {
        this.service = transactionService;
    }

    @PostMapping("/transaction")
    public void registerTransaction(@Valid @RequestBody TransactionRequest request) {
        service.registerTransaction(request);
    }
}
