package ch.selise.transactionmanager.support;

import ch.selise.transactionmanager.exception.InvalidRequestException;
import lombok.extern.slf4j.Slf4j;

import java.util.Base64;

/**
 * Base64 Util Class
 *
 * @author Badrul
 */
@Slf4j
public class Base64Utils {

    private Base64Utils() {
    }

    /**
     * @param s type String
     * @return decoded String value
     */
    public static String decode(String s) {
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            return new String(decoder.decode(s));
        } catch (Exception e) {
            log.error("Base64.Decoder failed: ", e);
            throw new InvalidRequestException("Base64.Decoder failed", e);
        }
    }
}
