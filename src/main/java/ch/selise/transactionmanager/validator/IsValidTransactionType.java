package ch.selise.transactionmanager.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Constraint(validatedBy = IsValidTransactionTypeValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface IsValidTransactionType {
    String message() default "should be either TRANSFER or REVERSE";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
