package ch.selise.transactionmanager.validator;


import ch.selise.transactionmanager.support.Base64Utils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IsValidTransactionTypeValidator implements ConstraintValidator<IsValidTransactionType, String> {
    @Override
    public void initialize(IsValidTransactionType constraintAnnotation) {
        // TODO document why this method is empty
    }

    @Override
    public boolean isValid(String type, ConstraintValidatorContext constraintValidatorContext) {
        try {
            String decodedType = Base64Utils.decode(type);
            return decodedType.equals("TRANSFER") || decodedType.equals("REVERSE");
        } catch (Exception e) {
            return false;
        }
    }
}
