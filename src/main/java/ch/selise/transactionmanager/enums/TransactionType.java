package ch.selise.transactionmanager.enums;

public enum TransactionType {
    TRANSFER,
    REVERSE;
}
