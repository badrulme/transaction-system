package ch.selise.transactionmanager.entity;

import ch.selise.transactionmanager.enums.TransactionType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "TRANSACTION")
public class TransactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "request_id", nullable = false)
    private String requestId;

    @Column(name = "transaction_time", nullable = false)
    private Date transactionTime;

    @Column(name = "requester", nullable = false)
    private String requester;

    @Column(name = "transaction_type", nullable = false, length = 10)
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    @OneToOne(optional = false)
    @JoinColumn(name = "source_account_number", nullable = false)
    private AccountEntity sourceAccountEntity;

    @Column(name = "amount", nullable = false)
    private Double amount;

    @OneToOne(optional = false)
    @JoinColumn(name = "destination_account_number", nullable = false)
    private AccountEntity destinationAccountEntity;

    @Column(name = "note", length = 500, nullable = false)
    private String note;

}
