package ch.selise.transactionmanager.repository;

import ch.selise.transactionmanager.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Integer> {

    AccountEntity findByAccountNumberAndStatusTrue(String accountNumber);
}
