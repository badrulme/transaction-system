package ch.selise.transactionmanager.repository;

import ch.selise.transactionmanager.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer> {

    Boolean existsByRequestId(String requestId);
}
