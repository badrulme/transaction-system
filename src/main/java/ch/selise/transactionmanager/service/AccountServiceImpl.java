package ch.selise.transactionmanager.service;

import ch.selise.transactionmanager.entity.AccountEntity;
import ch.selise.transactionmanager.exception.ResourceNotFoundException;
import ch.selise.transactionmanager.repository.AccountRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository repository;

    public AccountServiceImpl(AccountRepository repository) {
        this.repository = repository;
    }

    public AccountEntity updateAccountEntity(AccountEntity entity) {
        return repository.save(entity);
    }

    public AccountEntity getAccountEntityByAccount(String accountNumber) {
        AccountEntity accountEntity = repository.findByAccountNumberAndStatusTrue(accountNumber);
        if (!ObjectUtils.isEmpty(accountEntity)) {
            return accountEntity;
        } else {
            throw new ResourceNotFoundException("Account not found for: " + accountNumber);
        }
    }
}
