package ch.selise.transactionmanager.service;


import ch.selise.transactionmanager.entity.AccountEntity;

public interface AccountService {
    AccountEntity getAccountEntityByAccount(String accountNumber);

    AccountEntity updateAccountEntity(AccountEntity entity);
}
