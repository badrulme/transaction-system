package ch.selise.transactionmanager.service;


import ch.selise.transactionmanager.model.TransactionRequest;

public interface TransactionService {

    void registerTransaction(TransactionRequest request);


    Boolean existsByRequestId(String requestId);
}
