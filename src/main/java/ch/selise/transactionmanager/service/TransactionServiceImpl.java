package ch.selise.transactionmanager.service;

import ch.selise.transactionmanager.entity.AccountEntity;
import ch.selise.transactionmanager.entity.TransactionEntity;
import ch.selise.transactionmanager.enums.TransactionType;
import ch.selise.transactionmanager.exception.InvalidRequestException;
import ch.selise.transactionmanager.model.TransactionRequest;
import ch.selise.transactionmanager.repository.TransactionRepository;
import ch.selise.transactionmanager.support.Base64Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Transaction Service
 *
 * @author Badrul
 */
@Slf4j
@Service
public class TransactionServiceImpl implements TransactionService {
    private final AccountService accountService;
    private final TransactionRepository repository;

    /**
     * Dependency inject throw constructor autowiring
     *
     * @param accountService type of AccountService
     * @param repository     type of TransactionRepository
     */
    public TransactionServiceImpl(AccountService accountService, TransactionRepository repository) {
        this.accountService = accountService;
        this.repository = repository;
    }

    /**
     * Register Transaction details
     *
     * @param request type of TransactionRequest
     */
    public void registerTransaction(TransactionRequest request) {

        if (Boolean.TRUE.equals(existsByRequestId(request.getRequestId()))) {
            throw new InvalidRequestException("requestId already exists!");
        }

        Double amount;

        String amountInString = Base64Utils.decode(request.getAmount());

        try {
            amount = Double.valueOf(amountInString);
            if (amount <= 0) {
                throw new NumberFormatException("The amount should be greater than 0");
            }
        } catch (Exception e) {
            log.error("Amount conversion failed for: " + amountInString, e);
            throw new NumberFormatException("Amount conversion failed for : " + amountInString);
        }

        // Source account
        AccountEntity sourceAccountEntity = accountService
                .getAccountEntityByAccount(Base64Utils.decode(request.getSourceAccountNumber()));

        // Destination account
        AccountEntity destinationAccountEntity = accountService
                .getAccountEntityByAccount(Base64Utils.decode(request.getDestinationAccountNumber()));

        TransactionType transactionType = TransactionType.valueOf(Base64Utils.decode(request.getTransactionType()));

        if (TransactionType.TRANSFER.equals(transactionType)) {
            /* Business Logic for TransactionType.TRANSFER
                1. Debit the amount from source account balance
                2. Credit the amount to destination account balance in Account table
             */

            // 1. Debit the amount from source account balance
            if (sourceAccountEntity.getBalance() - amount < 0) {
                throw new InvalidRequestException("source account does not have sufficient balance!");
            }
            sourceAccountEntity.setBalance(sourceAccountEntity.getBalance() - amount);
            accountService.updateAccountEntity(sourceAccountEntity);

            // 2. Credit the amount to destination account balance in Account table
            destinationAccountEntity.setBalance(destinationAccountEntity.getBalance() + amount);
            accountService.updateAccountEntity(destinationAccountEntity);

        } else {
            /* Business Logic for TransactionType.REVERSE
                1. Debit the amount from destination account balance
                2. Credit the amount to source account balance in Account table
             */

            // 1. Debit the amount from destination account balance
            if (destinationAccountEntity.getBalance() - amount < 0) {
                throw new InvalidRequestException("destination account does not have sufficient balance!");
            }
            destinationAccountEntity.setBalance(destinationAccountEntity.getBalance() - amount);
            accountService.updateAccountEntity(destinationAccountEntity);

            // 1. Credit the amount to source account balance in Account table
            sourceAccountEntity.setBalance(sourceAccountEntity.getBalance() + amount);
            accountService.updateAccountEntity(sourceAccountEntity);
        }

        // Transaction details
        TransactionEntity entity = new TransactionEntity();

        entity.setRequestId(request.getRequestId());
        entity.setTransactionTime(new Date());
        entity.setRequester(request.getRequester());
        entity.setTransactionType(transactionType);
        entity.setSourceAccountEntity(sourceAccountEntity);
        entity.setAmount(amount);
        entity.setDestinationAccountEntity(destinationAccountEntity);
        entity.setNote(request.getNote());

        repository.save(entity);

    }


    /**
     * Exists Transaction By RequestId
     *
     * @param requestId type of String
     * @return Boolean
     */
    public Boolean existsByRequestId(String requestId) {
        return repository.existsByRequestId(requestId);
    }
}
