SET foreign_key_checks = 0;
drop table if exists ACCOUNT;

create table ACCOUNT
(
    id             int primary key auto_increment,
    name           varchar(255) not null,
    address        varchar(255),
    account_number varchar(255) not null unique,
    balance        double       not null,
    status         tinyint(1)   not null default false
);

insert into ACCOUNT (name, address, account_number, balance, status)
values ('Arifur Rahman', 'Dhaka, Bangladesh', '001241009211439', 5000, true);

insert into ACCOUNT (name, address, account_number, balance, status)
values ('Rakinul Huq', 'Dhaka, Bangladesh', '32341200923487', 5000, true);

insert into ACCOUNT (name, address, account_number, balance, status)
values ('Md. Badrul Alam', 'Jashore, Bangladesh', '123456789123456', 5000, true);

drop table if exists TRANSACTION;
create table TRANSACTION
(
    id                         int primary key auto_increment,
    request_id                 varchar(255) not null unique,
    transaction_time           datetime,
    requester                  varchar(255) not null,
    transaction_type           varchar(10) not null,
    source_account_number      int          not null,
    amount                     double,
    destination_account_number int          not null,
    note                       varchar(255) not null,
    foreign key TRANSACTION_FK_01 (source_account_number) references ACCOUNT (id),
    foreign key TRANSACTION_FK_02 (destination_account_number) references ACCOUNT (id)
);

SET foreign_key_checks = 1;